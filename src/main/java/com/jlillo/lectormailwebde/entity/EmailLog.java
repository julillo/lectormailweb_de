package com.jlillo.lectormailwebde.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
@Entity
@Table(name = "email_logs")
public class EmailLog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID", unique = true, nullable = false)
    private long id;

    @Column(name = "FROM_WHO", unique = false, nullable = false)
    private String from;

    @Column(name = "TO_WHO", unique = false, nullable = false)
    private String to;

    @Column(name = "SUBJECT", unique = false, nullable = true)
    private String subject;

    @Column(name = "ATTACHMENTS", length = 500, unique = false, nullable = true)
    private String attachments;

    @Column(name = "RECEIVED_AT", unique = false, nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date receivedDate;

    @Column(name = "CREATED_AT", unique = false, nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Date getCreated_at() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public static class Builder {

        private long _id;

        private String _from;

        private String _to;

        private String _subject = null;

        private String _attachments = null;

        private Date _received_at = null;

        private Date _created_at = new Date();

        public Builder id(long _id) {
            this._id = _id;
            return this;
        }

        public Builder from(String _from) {
            this._from = _from;
            return this;
        }

        public Builder to(String _to) {
            this._to = _to;
            return this;
        }

        public Builder subject(String _subject) {
            this._subject = _subject;
            return this;
        }

        public Builder attachments(String _attachments) {
            this._attachments = _attachments;
            return this;
        }

        public Builder receivedAt(Date _received_at) {
            this._received_at = _received_at;
            return this;
        }

        public Builder createdAt(Date _created_at) {
            this._created_at = _created_at;
            return this;
        }

        public EmailLog build() {
            return new EmailLog(this);
        }
    }

    public EmailLog() {
    }

    public EmailLog(Builder b) {
        this.id = b._id;
        this.from = b._from;
        this.to = b._to;
        this.subject = b._subject;
        this.attachments = b._attachments;
        this.receivedDate = b._received_at;
        this.createdDate = b._created_at;
    }

    @Override
    public String toString() {
        return "EmailLog{" + "id=" + id + ", from=" + from + ", to=" + to + ", subject=" + subject + ", attachments=" + attachments + ", receivedDate=" + receivedDate + ", createdDate=" + createdDate + '}';
    }

}
