package com.jlillo.lectormailwebde.utils.mail;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 * @since 2017
 */
public class Mail {

    private String host;
    private String user;
    private String password;
    private String storeType;
    private int storePort;
    private int smtpPort;
    private Properties properties;
    private Session emailSession;
    private Store store;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public int getStorePort() {
        return storePort;
    }

    public void setStorePort(int storePort) {
        this.storePort = storePort;
    }

    public int getSmtpPort() {
        return smtpPort;
    }

    public void setSmtpPort(int smtpPort) {
        this.smtpPort = smtpPort;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Session getEmailSession() {
        return emailSession;
    }

    public void setEmailSession(Session emailSession) {
        this.emailSession = emailSession;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public static class Builder {

        private String _host;
        private String _user;
        private String _password;
        private String _storeType;
        private int _storePort;
        private int _smtpPort;

        public Builder host(String _host) {
            this._host = _host;
            return this;
        }

        public Builder user(String _user) {
            this._user = _user;
            return this;
        }

        public Builder password(String _password) {
            this._password = _password;
            return this;
        }

        public Builder storeType(String _storeType) {
            this._storeType = _storeType;
            return this;
        }

        public Builder storePort(int _storePort) {
            this._storePort = _storePort;
            return this;
        }

        public Builder smtpPort(int _smtpPort) {
            this._smtpPort = _smtpPort;
            return this;
        }

        public Mail build() {
            return new Mail(this);
        }

    }

    public Mail(Builder b) {
        this.host = b._host;
        this.user = b._user;
        this.password = b._password;
        this.storeType = b._storeType;
        this.storePort = b._storePort;
        this.smtpPort = b._smtpPort;
    }

    public Mail() {

    }

    public boolean connect() {
        try {
            //create properties field
            this.properties = new Properties();

            properties.put(String.format("mail.%s.host", this.storeType), this.host);
//            properties.put(String.format("mail.%s.port", this.storeType), this.storePort);
//            properties.put(String.format("mail.%s.starttls.enable", this.storeType), "true");

            this.emailSession = Session.getInstance(this.properties);

            //create the POP3 store object and connect with the pop server
            switch (this.storeType) {
                case "pop3":
                    this.store = emailSession.getStore("pop3s");
                    break;
                default:
                    this.store = emailSession.getStore(this.storeType);
            }

            System.out.println(String.format("Connection data: [host: '%s', user: '%s', pass: '%s']", host, user, password));
            this.store.connect(this.host, this.user, this.password);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (MessagingException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean close() {
        try {
            store.close();
        } catch (MessagingException ex) {
            Logger.getLogger(Mail.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Mail{" + "host=" + host + ", user=" + user + ", password=" + password + ", storeType=" + storeType + ", storePort=" + storePort + ", smtpPort=" + smtpPort + ", properties=" + properties + ", emailSession=" + emailSession + ", store=" + store + '}';
    }

}
