package com.jlillo.lectormailwebde.utils.rut;

/**
 *
 * @author Juan Lillo <ju.lillo.rojas@gmail.com>
 */
public class Rut {

    private static int rut;
    private static char dv;

    public static String rutString(String input) {
        return split(input) != null ? (split(input))[0] : "";
    }
    
    public static int rutInt(String input){
        return split(input) != null ? Integer.parseInt((split(input))[0]) : 0;
    }

    public static String dv(String input) {
        return split(input) != null ? (split(input))[1] : "";
    }

    public static String[] split(String input) {
        String[] retorno = {Integer.toString(rut), String.valueOf(dv)};
        return validarRut(input) ? retorno : null;
    }

    public static boolean validarRut(String input) {

        boolean validacion = false;
        try {
            input = input.toUpperCase();
            input = input.replace(".", "");
            input = input.replace("-", "");
            rut = Integer.parseInt(input.substring(0, input.length() - 1));
            int rutAux = rut;

            dv = input.charAt(input.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}
